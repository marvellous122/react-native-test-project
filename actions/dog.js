import { GET_RANDOM_DOG } from "./types";

export const saveRandomDog = (url) => ({
  type: GET_RANDOM_DOG,
  url
});

export const get_random_dog = () => {
  return (dispatch, getState) => {
    fetch("https://dog.ceo/api/breeds/image/random", {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveRandomDog(responseJSON.message));
        })
    })
  };
};
