import { GET_RANDOM_DOG } from "../actions/types";

const defaultState = {
  breed: null,
  url: null
};

export default function reducer(state = defaultState, action) {

  switch (action.type) {
    case GET_RANDOM_DOG: {
      return {
        ...state,
        breed: "random",
        url: action.url
      };
    }

    default:
      return state;
  };
};
